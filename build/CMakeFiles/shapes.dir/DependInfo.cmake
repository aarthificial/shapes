# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/arthur/cpp/shapes/src/Client/Client.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/Client.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/Interaction.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/Interaction.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/State/AreaState.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/State/AreaState.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/State/EditState.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/State/EditState.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/State/InitialState.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/State/InitialState.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/State/InputState.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/State/InputState.cpp.o"
  "/home/arthur/cpp/shapes/src/Client/State/ReadyState.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Client/State/ReadyState.cpp.o"
  "/home/arthur/cpp/shapes/src/Shape/CircleShape.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Shape/CircleShape.cpp.o"
  "/home/arthur/cpp/shapes/src/Shape/RectangleShape.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/Shape/RectangleShape.cpp.o"
  "/home/arthur/cpp/shapes/src/main.cpp" "/home/arthur/cpp/shapes/build/CMakeFiles/shapes.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
