#include "Interaction.h"

Interaction::Interaction(Client &client) {
    state = new InitialState;
    this->client = &client;
}

int Interaction::begin() {
    while (!client->isHandled()) {
        AbstractState *nextState = state->handle(client);
        if (nextState != nullptr) {
            delete state;
            state = nextState;
        }
    }

    return 0;
}

