#include "Client.h"

bool Client::isHandled() {
    return handled;
}

void Client::setHandled(bool handled) {
    this->handled = handled;
}

bool Client::canAddShape() {
    return numberOfShapes < Client::MAX_SHAPES;
}

void Client::addShape(AbstractShape *shape) {
    if (canAddShape()) {
        shapes[numberOfShapes] = shape;
        numberOfShapes++;
    } else {
        delete shape;
    }
}

void Client::replaceShape(AbstractShape *shape, int index) {
    if (shapes[index] == nullptr) {
        delete shape;
    } else {
        delete shapes[index];
        shapes[index] = shape;
    }
}

AbstractShape *Client::getShape(int index) {
    return shapes[index];
}

int Client::getNumberOfShapes() {
    return numberOfShapes;
}
