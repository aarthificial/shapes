#pragma once

#include "Client.h"
#include "Client/State/AbstractState.h"
#include "Client/State/InitialState.h"
#include "Client/State/InputState.h"

class Interaction {
    Client *client;
    AbstractState *state;
public:
    Interaction(Client &client);

    int begin();
};


