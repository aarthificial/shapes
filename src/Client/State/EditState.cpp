#include "EditState.h"
#include "ReadyState.h"
#include <iostream>

using namespace std;

AbstractState *EditState::handle(Client *client) {
    bool success = false;
    int index;

    cout << "\nChoose shape:\n[0] Cancel\n";
    for (int i = 0; i < Client::MAX_SHAPES; i++) {
        AbstractShape *shape = client->getShape(i);
        if (shape != nullptr) {
            cout << "[" << i + 1 << "] " << shape->getName() << "\n";
        }
    }

    while (!success) {
        cin.clear();
        cin >> index;
        if (index == 0) {
            return new ReadyState;
        } else if (index > 0 && index <= Client::MAX_SHAPES) {
            index--;
            success = true;
        }
    }

    AbstractShape *newShape = client->getShape(index)->edit();
    if (newShape != nullptr) {
        client->replaceShape(newShape, index);
    }

    return new ReadyState;
}
