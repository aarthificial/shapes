#include "InputState.h"
#include "InitialState.h"
#include "ReadyState.h"
#include <iostream>
#include <Shape/RectangleShape.h>
#include <Shape/CircleShape.h>

using namespace std;

AbstractState *InputState::handle(Client *client) {
    if (!client->canAddShape()) {
        return new ReadyState;
    }

    bool success = false;
    char option;

    cout << "\nChoose ";
    if (client->getNumberOfShapes() > 0) {
        cout << "another ";
    }
    cout << "shape:\n[R]ectangle [C]ircle\n";

    while (!success) {
        cin.clear();
        cin >> option;
        switch (option) {
            case 'R':
            case 'r':
                client->addShape(RectangleShape::create());
                success = true;
                break;
            case 'C':
            case 'c':
                client->addShape(CircleShape::create());
                success = true;
                break;
        }
    }

    return new InputState;
}
