#pragma once

#include "AbstractState.h"

class EditState : public AbstractState {
    virtual AbstractState *handle(Client *client);
};


