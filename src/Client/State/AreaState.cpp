#include "AreaState.h"
#include "ReadyState.h"
#include <iostream>

using namespace std;

AbstractState *AreaState::handle(Client *client) {
    double area = 0;
    for (int i = 0; i < Client::MAX_SHAPES; i++) {
        AbstractShape *shape = client->getShape(i);
        if (shape != nullptr) {
            area += shape->calculateArea();
        }
    }

    cout << "\nTotal area:\n" << area << '\n';

    return new ReadyState;
}
