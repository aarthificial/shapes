#pragma once

#include "AbstractState.h"

class ReadyState : public AbstractState {
public:
    virtual AbstractState *handle(Client *client);
};


