#include "ReadyState.h"
#include "EditState.h"
#include "AreaState.h"
#include <iostream>

using namespace std;

AbstractState *ReadyState::handle(Client *client) {
    bool success = false;
    char option;
    AbstractState *result;

    cout << "\nWhat do you want to do?\n[E]dit shape   [C]alculate total area\n";

    while (!success) {
        cin.clear();
        cin >> option;
        switch(option) {
            case 'e':
            case 'E':
                result = new EditState;
                success = true;
                break;
            case 'c':
            case 'C':
                result = new AreaState;
                success = true;
                break;
        }
    }

    return result;
}
