#pragma once

#include "AbstractState.h"

class InputState : public AbstractState {
public:
    virtual AbstractState *handle(Client *client);
};


