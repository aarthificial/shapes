#pragma once

#include "AbstractState.h"

class AreaState : public AbstractState {
    virtual AbstractState *handle(Client *client);
};


