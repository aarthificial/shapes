#pragma once

#include "Client/Client.h"

class AbstractState {
public:
    virtual ~AbstractState() = default;
    virtual AbstractState *handle(Client *client) = 0;
};
