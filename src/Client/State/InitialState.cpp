#include "InitialState.h"
#include "InputState.h"
#include <iostream>

using namespace std;

AbstractState *InitialState::handle(Client *client) {
    cout << "Welcome\n";

    return new InputState;
}
