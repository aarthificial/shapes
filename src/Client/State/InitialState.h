#pragma once

#include "AbstractState.h"

class InitialState : public AbstractState {
public:
    virtual AbstractState *handle(Client *client);
};


