#pragma once

#include "Shape/AbstractShape.h"

class Client {
public:
    const static int MAX_SHAPES = 1;

private:
    bool handled = false;
    int numberOfShapes = 0;
    AbstractShape *shapes[Client::MAX_SHAPES] = {};

public:
    bool isHandled();

    void setHandled(bool handled);

    bool canAddShape();

    void addShape(AbstractShape *shape);

    void replaceShape(AbstractShape *shape, int index);

    AbstractShape *getShape(int index);

    int getNumberOfShapes();
};


