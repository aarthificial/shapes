#include <iostream>
#include "Client/Interaction.h"

int main() {
    Client client;
    Interaction interaction(client);
    return interaction.begin();
}