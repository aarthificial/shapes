#pragma once

#include <string>
#include "AbstractShape.h"

using namespace std;

class RectangleShape : public AbstractShape {
    static const string NAME;

    int width;
    int height;
public:
    static RectangleShape *create(bool editing = false);

    RectangleShape (int width, int height);

    virtual double calculateArea();

    virtual AbstractShape *edit();

    virtual string getName();
};


