#pragma once

#include <string>

using namespace std;

class AbstractShape {
public:
    virtual ~AbstractShape() = default;
    virtual double calculateArea() = 0;
    virtual AbstractShape *edit() = 0;
    virtual string getName() = 0;
};


