#include "RectangleShape.h"
#include <string>
#include <iostream>

using namespace std;

const string RectangleShape::NAME = "Rectangle";

RectangleShape *RectangleShape::create(bool editing) {
    bool success = false;
    int width;
    int height;
    RectangleShape *result;

    editing
        ? cout << "\nEditing rectangle\n"
        : cout << "\nCreating rectangle\n";

    while(!success) {
        cout << "Width: ";
        cin.clear();
        cin >> width;
        cout << "Height: ";
        cin.clear();
        cin >> height;

        try {
            result = new RectangleShape(width, height);
            success = true;
        } catch (const exception &error) {
            cout << '\n' << error.what() << "\nPlease try again\n";
        }
    }

    return result;
}

RectangleShape::RectangleShape(int width, int height) {
    if (width < 0) {
        throw runtime_error("Width cannot be less than 0. Got: " + to_string(width));
    }
    if (height < 0) {
        throw runtime_error("Height cannot be less than 0. Got: " + to_string(height));
    }

    this->width = width;
    this->height = height;
}

double RectangleShape::calculateArea() {
    return width * height;
}

AbstractShape *RectangleShape::edit() {
    return RectangleShape::create(true);
}

string RectangleShape::getName() {
    return RectangleShape::NAME + " " + to_string(width) + "x" + to_string(height);
}
