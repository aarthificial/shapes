#pragma once

#include "AbstractShape.h"
#include <string>

class CircleShape : public AbstractShape {
    static const string NAME;

    int radius;
public:
    static CircleShape *create(bool editing = false);

    CircleShape (int radius);

    virtual double calculateArea();

    virtual AbstractShape *edit();

    virtual string getName();
};


