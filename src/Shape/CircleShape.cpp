#define _USE_MATH_DEFINES

#include "CircleShape.h"
#include <string>
#include <iostream>
#include <math.h>

using namespace std;

const string CircleShape::NAME = "Circle";

CircleShape *CircleShape::create(bool editing) {
    bool success = false;
    int radius;
    CircleShape *result;

    editing
    ? cout << "\nEditing circle\n"
    : cout << "\nCreating circle\n";

    while(!success) {
        cout << "Radius: ";
        cin.clear();
        cin >> radius;

        try {
            result = new CircleShape(radius);
            success = true;
        } catch (const exception &error) {
            cout << '\n' << error.what() << "\nPlease try again\n";
        }
    }

    return result;
}

CircleShape::CircleShape(int radius) {
    if (radius < 0) {
        throw runtime_error("Radius cannot be less than 0. Got: " + to_string(radius));
    }

    this->radius = radius;
}

double CircleShape::calculateArea() {
    return radius * radius * M_PI;
}

AbstractShape *CircleShape::edit() {
    return CircleShape::create(true);
}

string CircleShape::getName() {
    return CircleShape::NAME + " " + to_string(radius);
}
